# Python Kafka Topic Consumer

Install dependencies 
```
pip install -r requirements.txt
```

Run script to dump kafka topic and save results to semicolon separated csv file
```
python consumer.py [topic]
```

Run script to compare messages from hiddenLive and live topics and save results of comparison to semicolon separated csv file
```
python compare.py [hiddenLiveData csv fileName] [liveData csv fileName]
```