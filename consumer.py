import os
import sys
import json
import warnings
import pandas as pd
from kafka import KafkaConsumer

warnings.filterwarnings("ignore")

if len(sys.argv) != 2:
    print("Usage: python consumer.py [kafka topic]")
    exit()
else:
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
    kafkaTopic = sys.argv[1]
    fileName = "%s.csv" % kafkaTopic

    df = pd.DataFrame(columns= ['key', 'offset', 'value'])

    consumer = KafkaConsumer(kafkaTopic,
                             bootstrap_servers=['b-5.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092',
                                                'b-1.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092',
                                                'b-4.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092',
                                                'b-6.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092',
                                                'b-2.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092',
                                                'b-3.trading-msk-dev.4i0wm2.c17.kafka.us-east-1.amazonaws.com:9092'],
                             auto_offset_reset='earliest',
                             enable_auto_commit=False,
                             key_deserializer=lambda key: key.decode('utf-8'),
                             value_deserializer=lambda msg: json.loads(msg.decode('utf-8')),
                             consumer_timeout_ms=10000)

    consumer.subscribe(kafkaTopic)
    
    print("Dumping kafka topic: %s..." % kafkaTopic)

    for message in consumer:
        row = pd.DataFrame({
            'key':[message.key],
            'offset':[message.offset],
            'value':[message.value]
        })
        df = pd.concat([df, row], ignore_index=True)

    if len(df.index) > 1:
        df.to_csv(fileName, index=False, encoding='utf-8', sep=';')
        print("Topic %s dumped and saved to %s/%s" % (kafkaTopic, script_dir, fileName))
    else: 
        print("Did not dump any message from topic: %s" % kafkaTopic)