from fileinput import filename
import os
import sys
import numpy as np
import pandas as pd

if len(sys.argv) != 3:
    print("Usage: python compare.py [hiddenLive fileName] [fileName]")
    exit()
else:
    dfA = pd.read_csv(sys.argv[1], sep=';')
    dfB = pd.read_csv(sys.argv[2], sep=';')
    fileName = '%s-comparison.csv' % sys.argv[2][:-4]

    print("Hidden Live topic input: ")
    print(dfA)
    print("Live topic input: ")
    print(dfB)

    # sort data frames 
    dfA = dfA.sort_values(by=['key', 'offset'], ascending=[True, False])
    dfA = dfA.drop_duplicates(subset='key')
    dfB = dfB.sort_values(by=['key', 'offset'], ascending=[True, False])
    dfB = dfB.drop_duplicates(subset='key')

    # merge data frames for hiddenLive keys
    dfA = dfA.merge(dfB, on='key', how='outer').drop(['offset_x'], axis=1).drop(['offset_y'], axis=1).fillna('')
    dfA.rename(columns = {'value_x':'value_hiddenLive', 'value_y':'value_live'}, inplace = True)

    # compare values
    dfA['equals'] = np.where(dfA['value_hiddenLive'] == dfA['value_live'], 'True', 'False')

    dfA.to_csv(fileName, index=False, encoding='utf-8', sep=';')
    print(dfA)
